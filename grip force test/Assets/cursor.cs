using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mudra.Unity;

public class cursor : MonoBehaviour
{

    public Transform Cursor;
    public GameObject discsManager;
    static Transform ctransform;
    static Disc disc;
    // Start is called before the first frame update
    int _screenWidth, _screenHeight;
    void Start()
    {
        ctransform = Cursor;
        _screenWidth = Camera.main.pixelWidth;
        _screenHeight = Camera.main.pixelHeight;

    }

    // Update is called once per frame
    void Update()
    {
        UpdateAirMouseEvent();
        var whichdisc = disc != null ? disc.serial : -1;
       // Debug.Log("disc: " + whichdisc);
 
    }
    private void OnTriggerEnter(Collider other)
    {
        
       if (other.GetComponent<Pole>())
          {
            if (discsManager != null)
                discsManager.GetComponent<DiscsManager>().detach_all_discs();
            if (disc!=null)
             if (disc.transform.position.y < 1.0f && disc.GetComponent<Rigidbody>().IsSleeping())
                disc = null;
            
             if (other.GetComponent<Pole>().discs_at_rest())
                  disc = null;
                var mydisc = other.GetComponent<Pole>().get_top_disc();
            if (mydisc != null && disc==null)
            {

                mydisc.can_attach = true;
            //    mydisc.set_cursor(this);
                Debug.Log("ener collision " + other.GetComponent<Pole>().serial +
                    " " + mydisc.serial);
            }
           }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Pole>())
        {
            if (other.GetComponent<Pole>().get_top_disc() != null)
            {
               //       other.GetComponent<Pole>().get_top_disc().can_attach = false;
               other.GetComponent<Pole>().unattach();
                if (discsManager!=null)
                   discsManager.GetComponent<DiscsManager>().detach_all_discs();
                Debug.Log("exit collision " + other.GetComponent<Pole>().serial +
                " " + other.GetComponent<Pole>().get_top_disc().serial);
            }
        }
    }

    static Vector3 mypos;
    public static void airmouse (Vector3 pos) {

        pos.x = Mathf.Min(pos.x, 7);
        pos.x = Mathf.Max(pos.x, -7);
        pos.y = ctransform.position.y;
        pos.z = ctransform.position.z;
        mypos = pos;
        ctransform.position = pos;
        //if (disc != null)
          //  disc.airmouse((float) pos.x);

    }

    static Vector3 _mouseScreenCoords;
    public float MouseSpeedX = 2.0f;
    public float MouseSpeedY = 6.0f;
    private void UpdateAirMouseEvent()
    {

        float[] delta = Plugin.Instance.GetLastAirMousePositionChange();
            if (delta != null)
            {
                _mouseScreenCoords.x += delta[0] * MouseSpeedX* _screenWidth;
                _mouseScreenCoords.y -= delta[1] * MouseSpeedY* _screenHeight;
                 _mouseScreenCoords.x= Mathf.Clamp(_mouseScreenCoords.x, 0.0f * _screenWidth, 1.0f * _screenWidth);
                _mouseScreenCoords.y = Mathf.Clamp(_mouseScreenCoords.y, 0.0f * _screenHeight, 1.0f * _screenHeight);
                _mouseScreenCoords.z= Camera.main.transform.position.z* -1.0f;
                airmouse(Camera.main.ScreenToWorldPoint(_mouseScreenCoords));
           
            }
        }
    public static Vector3 airmouse()
    {
        return mypos;
    }


    public static bool set_disc(Disc adisc)
    {
   
        if (disc == null)
               
        {
            disc = adisc;
            return true;
        }

        else  if ( disc.transform.position.y <1.0f && disc.GetComponent<Rigidbody>().IsSleeping())
        {
            disc = adisc;
            return true;
        }
        else
         return false;     

    }
    public static Disc get_disc()
    {
        return disc;
    }

    public static void remove_disc(Disc adisc)
    {
        if (disc == adisc)
            disc = null;
    }
    public static void hold_cursor(float posx)
    {
        Vector3 pos = ctransform.position;
        pos.x = posx;
        _mouseScreenCoords = Camera.main.WorldToScreenPoint( pos);
        ctransform.position = pos;

    }

}
