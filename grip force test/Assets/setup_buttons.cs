﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class setup_buttons : MonoBehaviour
{


    

    [System.Serializable]

    public class OnMenuEnabled : UnityEvent { }

    [SerializeField]
    private OnMenuEnabled setOnEnable = new OnMenuEnabled();

    // Start is called before the first frame update

    private void OnEnable()
    {
        setOnEnable.Invoke();
    }
}
