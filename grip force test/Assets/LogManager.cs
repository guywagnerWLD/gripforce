﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogManager : MonoBehaviour
{
    public GameObject log_panel,game_log_panel;
    static GameObject go,go1;
    static string Msg;
    // Start is called before the first frame update
    void Start()
    {
       go = log_panel;
        go1 = game_log_panel;
    }

    // Update is called once per frame
    void Update()
    {
        go.GetComponentInChildren<Text>().text = Msg;
    }

    public static void message(string s)
    {
        Msg = s;
        go.SetActive(true);
        go.GetComponentInChildren<Text>().text = s;

    }
    public static void GameMassage(string s)
    {
        Msg = s;
        go1.SetActive(true);
        go1.GetComponentInChildren<Text>().text = s;

    }
}
