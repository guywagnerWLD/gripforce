﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class selector_script : MonoBehaviour
{
    public GameObject[] colors;
    public OnSelect onselect;
    static int index;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void next()
    {
        index = (index + 1) % colors.Length;
        transform.position= colors[index].transform.position;
        onselect.Invoke(index);
    }
    public void select(int i)
    {
        index = i % colors.Length;
        transform.position = colors[index].transform.position;
        onselect.Invoke(index);
    }
    public void Reset()
    {
        index = 0;
        transform.position = colors[index].transform.position;
        onselect.Invoke(index);
    }
    [System.Serializable]
    public class OnSelect : UnityEvent<int>
    {
    }
}
