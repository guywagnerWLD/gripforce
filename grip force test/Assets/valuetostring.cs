﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class valuetostring : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void float_val(float val)    {

        GetComponentInChildren<Text>().text = val.ToString();

    }
    public void string_val(string val)
    {
        GetComponentInParent<Slider>().value = float.Parse(val);

    }

}
