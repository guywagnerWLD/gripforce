﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class SelectItem : MonoBehaviour
{
    public Dropdown dropdown;

    [System.Serializable]

    public class ButtonClickedEvent : UnityEvent<int> { }

    [SerializeField]
    private ButtonClickedEvent m_OnClick = new ButtonClickedEvent();

    // Start is called before the first frame update

    public void Next()
    {
        int n = dropdown.value;
        Image img;
        dropdown.TryGetComponent<Image>(out img);
        dropdown.SetValueWithoutNotify(0);
        if (n == 0)
        {

            img.color = Color.red;
            return;
        }
   
        img.color = Color.white;
        m_OnClick.Invoke(n);
    }
    private void OnEnable()
    {
      //  dropdown.SetValueWithoutNotify(0);
    }
}
