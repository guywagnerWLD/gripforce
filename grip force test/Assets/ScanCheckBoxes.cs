﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScanCheckBoxes : MonoBehaviour
{
    public GameObject[] CheckBoxes;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void run()
    {
        Toggle m_Toggle;
        foreach (GameObject go in CheckBoxes)
        {
            m_Toggle = go.GetComponent<Toggle>();
            m_Toggle.onValueChanged.Invoke(m_Toggle.isOn);

        }
    }
}
