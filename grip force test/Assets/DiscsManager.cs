﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Mudra.Unity;

public class DiscsManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject DiscPrefab;
    static GameObject discprefab;
    public GameObject PolePrefab;
    static GameObject poleprefab;
    public GameObject pressureBar;
    public GameObject PressureEvaluate;
    public OnGameOver onGameOver;
    public OnGameOver onSessionOver;
    public OnGameOver onLogOn;

    //   public List<Color> Poles;
    bool game_won = false;

    public Text timedisplay;
    public Text TaskTitle;
    public Slider timeslider;
    float timeLeft;
    float timeatstart;
    Color[] ColorSet = { Color.red, Color.green, Color.blue, Color.yellow, Color.magenta, Color.cyan, Color.black };
    // public static UnityEngine.Object prefab = Resources.Load(Prefab);
    //public List<DiscParams> Discs;
    List<Disc> discs = new List<Disc>();
    List<Pole> poles = new List<Pole>();    
    static Session games = new Session();
    static SessionsList AllSessions = new SessionsList();

    public static float GameTimeInSec;
    public int NumberOfGames = 1;
    static bool ReflectDiscSize = true;
    static string MissionStatement = "";
    static bool display_pressure_bar =true;
    static public bool Order_Weight = false;
    public bool GameRunning = true;
    static int current_game = -1;
    static public bool repeatOnfail = false;
    public static bool testPrecievedPressure = false;
     bool PressureBarManualOveride = false;
    bool PressureBarManualOverideValue = false;
    int disc_measured = 0;

   
    void Start() {


        discprefab = DiscPrefab;
        poleprefab = PolePrefab;
        timeatstart = Time.time;
        timeLeft = GameTimeInSec;
        timeslider.maxValue = GameTimeInSec;


    }

    public static void LoadSessions()
    {
        string json = FilesManager.Load_sessions();
        if (json != null)
        {
            AllSessions = JsonUtility.FromJson<SessionsList>(json);
        }
    }

    // Update is called once per frame
    void Update()
    {

    
        if (current_game < 0)
            return;
        if (GameRunning && !games.trials[current_game].testPrecievedPressure)
        {
            if (TaskTitle != null)
                TaskTitle.text = games.trials[current_game].MissionStatement;
            bool pause_clock = true;
            foreach (Disc d in discs)
                pause_clock = d.discOk() && pause_clock;

            if (pause_clock)
             timeLeft -= Time.deltaTime;
            if (timeLeft > 0 && poles.Count > 0 && discs.Count > 0)
            {
                timedisplay.text = timeLeft.ToString();
                timeslider.value = timeLeft;
                Pole lastpole = poles[poles.Count - 1];
                if (lastpole.disc_count() == discs.Count)
                {
                    game_won = Order_Weight ? lastpole.check_order_by_serial() : lastpole.check_order_by_weight();
                    game_over(game_won);
                }
            }
            else
            {
                game_won = false;
                FilesManager.set_status("Time Out");
                game_over(true);
            }
        }

    }

    public void pressureBarManualOveride() {

        PressureBarManualOveride = true;
    }

    //  public List<float> precieved_values = new List<float>();
    public void measure_next_disc()
    {
        Debug.Log("************disc_measured " + disc_measured.ToString() + "  " + discs[disc_measured].Raise.ToString() + " " + PressureEvaluate.GetComponent<Slider>().value.ToString() + games.trials[current_game].discsParams[disc_measured].Raise.ToString());
        FilesManager.add_percived_measuerment(disc_measured, discs[disc_measured].Raise, PressureEvaluate.GetComponent<Slider>().value);
        if (disc_measured + 1< discs.Count)
             PressureEvaluate.GetComponent<SliderMng>().set_color(discs[disc_measured+1].DiscColor);
       else
            game_over(true);
        disc_measured++;

    }
    public void detach_all_discs()
    {
        foreach (Disc d in discs)
        {
            d.can_attach = false;
        }

    }
    void game_over(bool gameover)
    {
        if (games.trials[current_game].testPrecievedPressure)
            PressureEvaluate.SetActive(false);
        if (gameover)
        {
            GameRunning = false;
            string msg = game_won ? "Well Done - You successfuly rearranged  the discs" : "You ran out of time - game over!";
            FilesManager.set_status(msg);
            FilesManager.stop_saving_game();
            LogManager.GameMassage(msg);

            while (discs.Count > 0)
                RemoveDisc();

            while (poles.Count > 0)
                RemovePole();
            onLogOn.Invoke();
        }
    }

    public void gameOver(bool nextOrRepeat) { 

            if (current_game>=0)
              if (!game_won && games.trials[current_game].repeatOnFail || nextOrRepeat)
                      
                    current_game -= 1;


            if (current_game >= games.trials.Count - 1 && games.trials.Count > 0 && !games.trials[current_game].repeatOnFail && !nextOrRepeat)
            {
                GameRunning = false;
                current_game = -1;
                games.trials.Clear();
                onSessionOver.Invoke();
            }
            else
            {

                GameRunning = false;
                onGameOver.Invoke();
            }
        
    }
    public void toggle_game()
    {


        if (GameRunning)
        {
            FilesManager.set_status("game stopped");
            GameRunning = false;

                while (discs.Count > 0)
                    RemoveDisc();

                while (poles.Count > 0)
                    RemovePole();
            if (current_game >= games.trials.Count - 1 && games.trials.Count > 0 && !games.trials[current_game].repeatOnFail)
            {
                current_game = -1;

                games.trials.Clear();
                onSessionOver.Invoke();
            }
            else if(games.trials[current_game].repeatOnFail && current_game >= games.trials.Count - 1 && games.trials.Count > 0)
            {
                current_game = current_game - 1;
            }
        }
        else
        {
            timeatstart = Time.time;
            timeLeft = GameTimeInSec;
            timeslider.maxValue= GameTimeInSec;
            foreach (Disc d in discs)
                FilesManager.add_transform(d.transform);
            GameRunning = true;            
        }
        if (testPrecievedPressure)
        {
            disc_measured = 0;
            if (GameRunning)
            {
                PressureEvaluate.SetActive(true);
                PressureEvaluate.GetComponent<SliderMng>().set_color(discs[disc_measured].DiscColor);
            }
            else PressureEvaluate.SetActive(false);
        }
        else
            PressureEvaluate.SetActive(false);
    }
    public  Disc Create(DiscParams discparams)
    {
        GameObject newObject = Instantiate(discprefab) as GameObject;
        newObject.name = "Disc#_"+ discparams.serial+ "TH_"+discparams.Raise.ToString("f2")+ "_TL_" + discparams.release.ToString("f2") + "_TB_" + discparams.BreakPoint.ToString("f2") ;
        Disc disc = newObject.GetComponent<Disc>();
        disc.serial = discparams.serial;
        disc.DiscColor = discparams.DiscColor;
        disc.Raise = discparams.Raise;
        disc.release = discparams.release;
        disc.BreakPoint = discparams.BreakPoint;
        disc.DiscSize=Mathf.Min(3,14/(poles.Count-1)/3);
        disc.relect_raise = ReflectDiscSize;




        return disc;
    }

    public  Pole Create(PoleParams poleparams)
    {
        GameObject newObject = Instantiate(poleprefab) as GameObject;
        Pole pole = newObject.GetComponent<Pole>();
        pole.serial = poleparams.serial;
        pole.PoleColor = poleparams.PoleColor;
        pole.xpos = poleparams.xpos;
        return pole;
    }

    public bool discs_at_rest()
    {
        bool is_at_rest = true;
        foreach(Disc d in discs)
        {
            is_at_rest= is_at_rest& d.is_at_rest();
        }
        return is_at_rest;
    }

    

    [Serializable]
    public struct DiscParams
    {
        public int serial;
        public Color DiscColor;
        public float Raise;
        public float release;
        public float BreakPoint;
    }

    [Serializable]
    public struct PoleParams
    {
        public int serial;
        public Color PoleColor;
        public float xpos;
    }



   
    public void SetupPoles(int npoles)
    {
       // npoles += 2;
       npoles=npoles >=2?npoles: 2;
        poleprefab = PolePrefab;
        foreach (Pole pole in poles)
        {
            Destroy(pole.gameObject);
           
        }
        poles.Clear();
        for (int i=0; i< npoles;i++)
        {
            PoleParams polparams = new PoleParams();
            polparams.PoleColor = ColorSet[i];
            polparams.serial = i;
            if (npoles == 1)
                polparams.xpos = -7;
            else
                polparams.xpos = 14 / (npoles - 1) * i - 7;
            
            poles.Add(Create(polparams));          
        }

    }

    public void add_Pole()
    {
        int npoles = poles.Count >= 1 ? poles.Count + 1:2 ;
        poleprefab = PolePrefab;
        foreach (Pole pole in poles)
        {
            Destroy(pole.gameObject);

        }
        poles.Clear();
        for (int i = 0; i < npoles; i++)
        {
            PoleParams polparams = new PoleParams();
            polparams.PoleColor = ColorSet[i];
            polparams.serial = i;
            if (npoles == 1)
                polparams.xpos = -7;
            else
                polparams.xpos = 14 / (npoles - 1) * i - 7;

            poles.Add(Create(polparams));          
        }

    }

    DiscParams newdisc;

    public void set_disc_raise(float raise)
    {
        newdisc.Raise = raise;


    }
    public void set_disc_release(float release)
    {  
        newdisc.release = release;
    }
    public void set_disc_breakpoint(float breakpoint)
    {
        newdisc.BreakPoint = breakpoint;
    }

    public void set_disc_color(int iColor)
    {
        newdisc.DiscColor = ColorSet[iColor];
    }


    public void add_disc()
    {
        newdisc.serial = discs.Count;
     //   newdisc.DiscColor = ColorSet[discs.Count];
        discs.Add(Create(newdisc));
    }

    public void add_disc(DiscParams dp)
    {
        dp.serial = discs.Count;
     //   dp.DiscColor = ColorSet[discs.Count];
        discs.Add(Create(dp));
    }
    public void RemoveDisc() {
        if (discs.Count > 0)
            Destroy(discs[discs.Count - 1].gameObject);
        discs.RemoveAt(discs.Count - 1);
    }
    public void GuiRemovePole()
    {
        int npoles = poles.Count >2 ? poles.Count - 1 : 2;
        poleprefab = PolePrefab;
        foreach (Pole pole in poles)
        {
            Destroy(pole.gameObject);

        }
        poles.Clear();
        for (int i = 0; i < npoles; i++)
        {
            PoleParams polparams = new PoleParams();
            polparams.PoleColor = ColorSet[i];
            polparams.serial = i;
            if (npoles == 1)
                polparams.xpos = -7;
            else
                polparams.xpos = 14 / (npoles - 1) * i - 7;

            poles.Add(Create(polparams));
        }
    }


    void RemovePole()
    {
        if (poles.Count > 0)
            Destroy(poles[poles.Count - 1].gameObject);
        poles.RemoveAt(poles.Count - 1);
    }
    public void SetGameTime(string game_time)
    {
        GameTimeInSec = float.Parse(game_time);
    }
    static public String getGameTime()
    {
        return GameTimeInSec.ToString();
    }
    public void setMissionStatement(string missionStatement)
    {
        MissionStatement = missionStatement;
    }
    static public string getMissionStatement()
    {
        return MissionStatement;
    }
    public void setShow_pressure_bar(bool show)
    {
        display_pressure_bar = show;
    }

    static public bool getShow_pressure_bar()
    {
        return display_pressure_bar;
    }

    public void setReflectDiscSize(bool reflect)
    {
        ReflectDiscSize = reflect;
    }
    static public bool getReflectDiscSize()
    {
        return ReflectDiscSize;
    }

    public void setOrder_Weight(bool Ow)
    {
        Order_Weight = Ow;
    }

    static public bool getOrder_Weight()
    {
        return Order_Weight;
    }

    static public void setRepeatOnFail(bool rep)
    {
        repeatOnfail = rep;
    }

    static public bool getRepeatOnFail()
    {
        return repeatOnfail;
    }
    public void setTestprecievedpressure(bool test)
    {
        testPrecievedPressure = test;
    }

    static public bool getTestprecievedpressure()
    {
        return testPrecievedPressure;
    }

    public void AddGame()
    {
        TrialParams game = new TrialParams();
        foreach (Disc aDisc in discs)
        {
            DiscParams dp=new DiscParams();
            dp.BreakPoint = aDisc.BreakPoint;
            dp.Raise = aDisc.Raise;
            dp.release = aDisc.release;
            dp.DiscColor = aDisc.DiscColor;
            game.add_discParams(dp);
        }
        foreach (Pole aPole in poles)
        {
            PoleParams pp = new PoleParams();            
            game.add_poleParams(pp);
        }
        game.GameTimeInSec = GameTimeInSec;
        game.ReflectDiscSize = ReflectDiscSize;
        game.display_pressure_bar = display_pressure_bar;
        game.Order_Weight = Order_Weight;
        game.testPrecievedPressure = testPrecievedPressure;
        game.repeatOnFail = repeatOnfail;
        game.MissionStatement = MissionStatement;
        games.trials.Add(game);      
    }

    public void setSessionName(string name)
    {
        games.Name=  name;
    }
    public void setSessionName_And_Save()
    {
        if (games.Name == null)
        {
            LogManager.message("please name the session");
            return;
        }
        foreach (Session s in AllSessions.sessionsList)
            if (s.Name == games.Name)
            {
                LogManager.message("Session " + games.Name + " Allready exist");
                clearSession();
                return;
            }

        Session asession = new Session();
        asession.Name = games.Name;
        //games.Name = name;

        foreach (TrialParams tp in games.trials)
        {
            asession.trials.Add(tp);
        }
        AllSessions.sessionsList.Add(asession);
        string json = JsonUtility.ToJson(AllSessions);
        FilesManager.save_sessions(json);
        save_sessions_log();
        clearSession();
    }

    public void replace_trial_in_session()
    {
        AllSessions.sessionsList[games.index].trials[current_game].discsParams.Clear();
        AllSessions.sessionsList[games.index].trials[current_game].polesParams.Clear();
        
        foreach (Disc aDisc in discs)
        {
            DiscParams dp = new DiscParams();
            dp.BreakPoint = aDisc.BreakPoint;
            dp.Raise = aDisc.Raise;
            dp.release = aDisc.release;
            dp.DiscColor = aDisc.DiscColor;
            AllSessions.sessionsList[games.index].trials[current_game].add_discParams(dp);
        }
        foreach (Pole aPole in poles)
        {
            PoleParams pp = new PoleParams();
            AllSessions.sessionsList[games.index].trials[current_game].add_poleParams(pp);
        }
        AllSessions.sessionsList[games.index].trials[current_game].GameTimeInSec = GameTimeInSec;
        AllSessions.sessionsList[games.index].trials[current_game].ReflectDiscSize = ReflectDiscSize;
        AllSessions.sessionsList[games.index].trials[current_game].display_pressure_bar = display_pressure_bar;
        AllSessions.sessionsList[games.index].trials[current_game].Order_Weight = Order_Weight;
        AllSessions.sessionsList[games.index].trials[current_game].testPrecievedPressure = testPrecievedPressure;
        AllSessions.sessionsList[games.index].trials[current_game].repeatOnFail = repeatOnfail;
        AllSessions.sessionsList[games.index].trials[current_game].MissionStatement = MissionStatement;
        string json = JsonUtility.ToJson(AllSessions);
        FilesManager.save_sessions(json);
        save_sessions_log();
        
        clearSession();
    }
    public void RemoveSession(int i) {
        AllSessions.sessionsList.RemoveAt(i-1);
        string json = JsonUtility.ToJson(AllSessions);
        FilesManager.save_sessions(json);
        save_sessions_log();
    }

public void RemoveTrial(int i)
    {
        //AllSessions.sessionsList[]
        games.trials.RemoveAt(i-1);
        AllSessions.sessionsList[games.index].trials.Clear();
        foreach (TrialParams tp in games.trials )
            AllSessions.sessionsList[games.index].trials.Add(tp);        
        string json = JsonUtility.ToJson(AllSessions);
        FilesManager.save_sessions(json);
        save_sessions_log();

    }

    public void moveTrialUp(int i)
    {
        //AllSessions.sessionsList[]
        if (i > 1) {
            games.trials.Insert(i - 2, games.trials[i - 1]);
            RemoveTrial(i+1);

        }

    }

    public void moveTrialDn(int i)
    {
        //AllSessions.sessionsList[]
        if (i< games.trials.Count-1)
        {
            games.trials.Insert(i, games.trials[i - 1]);
            RemoveTrial(i+1);

        }

    }

    public void AddEmpty(int i)
    {
        //AllSessions.sessionsList[]
      
            TrialParams tp = new TrialParams();
            tp.MissionStatement = "new";
            games.trials.Insert(i, tp);
        AllSessions.sessionsList[games.index].trials.Clear();
        foreach (TrialParams t in games.trials)
            AllSessions.sessionsList[games.index].trials.Add(t);
        string json = JsonUtility.ToJson(AllSessions);
        FilesManager.save_sessions(json);
        save_sessions_log();




    }

    public void LoadEditedTryal(int ii)
    {
        ii = ii - 1;
        current_game = ii;
        if (ii < games.trials.Count && games.trials.Count > 0)
        {
            while (discs.Count > 0)
                RemoveDisc();

            while (poles.Count > 0)
                RemovePole();

            GameTimeInSec = games.trials[current_game].GameTimeInSec;
            ReflectDiscSize = games.trials[current_game].ReflectDiscSize;
            display_pressure_bar = games.trials[current_game].display_pressure_bar;
            Order_Weight = games.trials[current_game].Order_Weight;
            testPrecievedPressure = games.trials[current_game].testPrecievedPressure;
            MissionStatement = games.trials[current_game].MissionStatement;
            if (TaskTitle != null)
                TaskTitle.text = games.trials[current_game].MissionStatement;
            SetupPoles(games.trials[current_game].polesParams.Count);
            int i = 1;
            foreach (DiscParams aDisc in games.trials[current_game].discsParams)
            {
                add_disc(aDisc);
                //                Invoke("addnextdisc", i);
                i += 2;

            }


        }

    }
    public void clearSession()
    {
        while (discs.Count > 0)
            RemoveDisc();

        while (poles.Count > 0)
            RemovePole();
        games.trials.Clear();
    }




void save_sessions_log()
    {
        string sessions_log = "index,name,trial index,time,ReflectDiscSize,display_pressure_bar,order by weight, repeat on fail,test Precieved Pressure," +
                                 "number of poles,disc index,rase,release,break,\n";
        string sIndex, sName,trial_log;
        foreach(Session s in AllSessions.sessionsList)
        {
            sIndex = s.index.ToString();
            sName = s.Name;
            int trialIndex = 0;
            foreach (TrialParams t in s.trials)
            {
                trial_log = sIndex + "," + sName + "," + trialIndex.ToString() + "," + t.GameTimeInSec.ToString() + "," + t.ReflectDiscSize.ToString() + "," +
                    t.display_pressure_bar.ToString() + "," + t.Order_Weight.ToString() + "," + t.repeatOnFail.ToString() + "," + t.testPrecievedPressure.ToString() + "," +
                    t.polesParams.Count.ToString();
                trialIndex++;
                int discIndex = 0;
                foreach(DiscParams d in t.discsParams)
                {
                    sessions_log += trial_log + "," + discIndex.ToString() + "," + d.Raise.ToString() + "," + d.release.ToString() + "," + d.BreakPoint.ToString() + "\n";
                    discIndex++;
                }
            }
        }
        FilesManager.save_sessions_log(sessions_log);

    }

    static public List<String> get_sessions()
    {
        List<String> sl = new List<String>() ;
        foreach (Session s in AllSessions.sessionsList)
            sl.Add(s.Name);
        return sl;
    }

    static public List<String> get_trials()
    {
        List<String> sl = new List<String>();

        foreach (TrialParams tp  in games.trials)
            sl.Add(tp.MissionStatement);
        return sl;
    }
    public static string get_current_game_name()
    {

        return (games.Name);
    }

    public static bool is_percived_pressure_test()
    {
       
        return testPrecievedPressure;
    }

    public static int get_current_trial()
    {
        return current_game;
    }

    public static int get_current_game_index()
    {
        return games.index;
    }
    public void LoadSession (int n)
    {
        n = n - 1;
        if (n < AllSessions.sessionsList.Count) { 

            games.Name = AllSessions.sessionsList[n].Name;
            games.index = n;
            games.trials.Clear();
            foreach (TrialParams tp in AllSessions.sessionsList[n].trials)
                games.trials.Add(tp);
            current_game = -1;
            if (TaskTitle != null && current_game >= -1)
                TaskTitle.text = games.trials[current_game+1].MissionStatement;
            //    LoadNextGame();
        }
    }
    public void LoadNextGame()
    {
        current_game++;
        if (current_game < games.trials.Count && games.trials.Count > 0)
        {
            while (discs.Count > 0)
                RemoveDisc();

            while (poles.Count > 0)
                RemovePole();

            GameTimeInSec = games.trials[current_game].GameTimeInSec;
            ReflectDiscSize = games.trials[current_game].ReflectDiscSize;
            display_pressure_bar = games.trials[current_game].display_pressure_bar;
            Order_Weight = games.trials[current_game].Order_Weight;
            testPrecievedPressure = games.trials[current_game].testPrecievedPressure;
            MissionStatement = games.trials[current_game].MissionStatement;
            if (TaskTitle!=null)
                TaskTitle.text = games.trials[current_game].MissionStatement;
            SetupPoles(games.trials[current_game].polesParams.Count);
            int i = 1;
            foreach (DiscParams aDisc in games.trials[current_game].discsParams)
            {
                add_disc(aDisc);
                //                Invoke("addnextdisc", i);
                i += 2;

            }
            if (!PressureBarManualOveride)
                pressureBar.SetActive(display_pressure_bar);
            else
            {
                PressureBarManualOveride = false;
            }
            
        }

    /*    else {
            while (discs.Count > 0)
                RemoveDisc();

            while (poles.Count > 0)
                RemovePole();
            current_game = -1;
            games.trials.Clear();
            onSessionOver.Invoke();
        }*/
       
    }



    int next_disc = 0;
    void addnextdisc()
    {
        if (next_disc < games.trials[current_game].discsParams.Count) {
            add_disc(games.trials[current_game].discsParams[next_disc]);
            next_disc++;
        }
    }

    [Serializable]
    public class TrialParams
    {
        public List<DiscParams> discsParams = new List<DiscParams>();
        public List<PoleParams> polesParams = new List<PoleParams>();           
        public float GameTimeInSec;
        public bool ReflectDiscSize = true;
        public bool display_pressure_bar = true;
        public bool Order_Weight = false;          
        public bool repeatOnFail = false;
        public bool testPrecievedPressure = false;
        public string MissionStatement = "";


        public void add_discParams(DiscParams aDisc)
        {
            discsParams.Add(aDisc);
        }
        public void add_poleParams(PoleParams aPole)
        {
            polesParams.Add(aPole);
        }      
                      
    }

    
    

    [Serializable]
    public class Session
    {
        public List<TrialParams> trials = new List<TrialParams>();
        public string Name;
        public int index;
    }

    [Serializable]
    public class SessionsList
    {
        public List<Session> sessionsList = new List<Session>();
    }
    
     

    [System.Serializable]
    public class OnGameOver : UnityEvent
    {
    }
}
