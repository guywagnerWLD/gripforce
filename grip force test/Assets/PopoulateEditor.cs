﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopoulateEditor : MonoBehaviour
{
    public GameObject TrialLengthsec;
    public Toggle onFailureRepeatTrial;
    public Toggle DiscSizeReflectsPressure;
    public Toggle ShowPressureBar;
    public Toggle GameEndsByWeight_order;
    public Toggle TestPercivedPressure;
    public GameObject TaskDescription;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    private void OnEnable()
    {
        TrialLengthsec.GetComponentInChildren<Text>().text = DiscsManager.getGameTime();
        onFailureRepeatTrial.isOn = DiscsManager.getRepeatOnFail();
        DiscSizeReflectsPressure.isOn = DiscsManager.getReflectDiscSize();
        ShowPressureBar.isOn = DiscsManager.getShow_pressure_bar();
        GameEndsByWeight_order.isOn = DiscsManager.getOrder_Weight();
        TestPercivedPressure.isOn = DiscsManager.getTestprecievedpressure();
        TaskDescription.GetComponentInChildren<Text>().text = DiscsManager.getMissionStatement();

    }
}
