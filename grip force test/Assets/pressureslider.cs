﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mudra.Unity;

public class pressureslider : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float? fingerTipPressure = Plugin.Instance.GetLastFingerTipPressure();
        if (fingerTipPressure != null)
            GetComponent<Slider>().value  = fingerTipPressure.Value*ParticipantsManager.getCalibration();       
    }
}
