﻿using Mudra.Unity;
using System;
using UnityEngine;

namespace Mudra
{
    sealed class MudraUnityAndroidPlugin : PluginPlatform
    {
        #region Android variables
        static AndroidJavaClass _mudraClass;
        static AndroidJavaObject _mudraCalibration;
        static AndroidJavaObject _mudraDevice;
        public bool _isConnected = false;
        String[] mycal;

        #endregion

        public override void Init(string calibrationFile = "")
        {
            Logger.Print("Mudra Android Unity Plugin Init started");

           _mudraClass = new AndroidJavaClass("MudraAndroidSDK.Mudra");
            AndroidJavaClass feature = new AndroidJavaClass("MudraAndroidSDK.Feature");

            AndroidJavaClass jcu = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

            AndroidJavaObject jo = jcu.GetStatic<AndroidJavaObject>("currentActivity");

            AndroidJavaObject context = jo.Call<AndroidJavaObject>("getApplicationContext");

            _mudraDevice = _mudraClass.CallStatic<AndroidJavaObject>("autoConnectPaired", context);

            _mudraCalibration = _mudraDevice.Call<AndroidJavaObject>("getCalibration");

            mycal =_mudraCalibration.Call<String[]>("loadLastCalibration");
      


            String LICENSE = "Feature::RawData";
            _mudraDevice.Call("setLicense", 0, LICENSE);
            String LICENSE_DC = "Feature::DoubleTap";
            _mudraDevice.Call("setLicense", 2, LICENSE_DC);

            UpdateOnGestureReadyCallback();
            UpdateOnFingerTipPressureCallback();
            UpdateAirMousePositionChangedCallback();
            UpdateOnQuaternionReadyCallback();

            Logger.Print("Mudra Android Unity Plugin Init done");
        }

        override public void Close()
        {
        }

        void OnLoggingMessage(string msg)
        {
            Logger.Print(msg);
        }

        #region OnGestureReady

        class OnGestureReady : AndroidJavaProxy
        {
            MudraUnityAndroidPlugin _unityPlugin;
            public OnGestureReady(MudraUnityAndroidPlugin unityPlugin) : base("MudraAndroidSDK.Mudra$OnGestureReady") { _unityPlugin = unityPlugin; }

            void run(AndroidJavaObject retObj)
            {

                _unityPlugin.SetLastGesture((GestureType)retObj.Call<int>("ordinal"));
            }
        }

        override protected void UpdateOnGestureReadyCallback()
        {
            if (_isGestureEnabled)
            {
                _mudraDevice.Call("setOnGestureReady", new OnGestureReady(this));
            }
            else
            {
                _mudraDevice.Call("setOnGestureReady", null);
            }
        }
        #endregion

        #region OnFingerTipPressureReady

        class OnFingertipPressureReady : AndroidJavaProxy
        {
            MudraUnityAndroidPlugin _unityPlugin;

            public OnFingertipPressureReady(MudraUnityAndroidPlugin unityplugin) : base("MudraAndroidSDK.Mudra$OnFingertipPressureReady") { _unityPlugin = unityplugin; }

            void run(float pressure)
            {
                _unityPlugin.SetLastFingerTipPressure(pressure);
            }
        }

        override protected void UpdateOnFingerTipPressureCallback()
        {
            if (_isFingerTipPressureEnabled)
                _mudraDevice.Call("setOnFingertipPressureReady", new OnFingertipPressureReady(this));
            else
                _mudraDevice.Call("setOnFingertipPressureReady", null);
        }
        #endregion

        #region OnAirMousePositionChanged

        class OnAirMousePositionChanged : AndroidJavaProxy
        {
            MudraUnityAndroidPlugin _unityPlugin;

            public OnAirMousePositionChanged(MudraUnityAndroidPlugin unityplugin) : base("MudraAndroidSDK.Mudra$OnAirMousePositionChanged") { _unityPlugin = unityplugin; }

            void run(float[] positionChanged)
            {
                _unityPlugin.SetLastAirMousePositionChange(positionChanged);
            }
        }
       
        override protected void UpdateAirMousePositionChangedCallback()
        {
            if (_isAirMouseEnabled)
                _mudraDevice.Call("setOnAirMousePositionChanged", new OnAirMousePositionChanged(this));
            else
                _mudraDevice.Call("setOnAirMousePositionChanged", null);
        }
        #endregion

        #region OnQuaternionReady

        class OnImuQuaternionReady : AndroidJavaProxy
        {
            MudraUnityAndroidPlugin _unityPlugin;
            public OnImuQuaternionReady(MudraUnityAndroidPlugin unityPlugin) : base("MudraAndroidSDK.Mudra$OnImuQuaternionReady") { _unityPlugin = unityPlugin; }

            void run(long ts,float[] q)
            {
                Quaternion quaternion = new Quaternion(q[0], q[1], q[2], q[3]);

                _unityPlugin.SetLastQuaternion(quaternion);
            }
        }
           
        override protected void UpdateOnQuaternionReadyCallback()
        {
            if (_isImuQuaternionEnabled)
                _mudraDevice.Call("setOnImuQuaternionReady", new OnImuQuaternionReady(this));
            else
                _mudraDevice.Call("setOnImuQuaternionReady", null);
        }
        #endregion

        public override bool IsConnected
        {
            get { return _isConnected; }
        }

        public override void Update()
        {
            _isConnected = _mudraDevice.Call<bool>("isConnected");
        }
    }
}


