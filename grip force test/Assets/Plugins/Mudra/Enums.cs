﻿namespace Mudra.SDK.Windows.DotNet
{
    public enum HandType
    {
        Left,
        Right
    };

    public enum GestureType
    {
        None,
		Thumb,
		Index,
		Tap,
		DoubleTap
	};

    public enum Feature
    {
        RawData,
        TensorFlowData,
        DoubleTap
    };

    public enum LoggingSeverity
    {
        Debug,
		Info,
		Warning,
		Error
    };

}