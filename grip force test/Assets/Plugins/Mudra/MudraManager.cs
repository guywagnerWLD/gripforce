﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Mudra.Unity
{
    public class MudraManager : MonoBehaviour
    {
        public string CalibrationFilePath = "C:\\Program Files\\Wearable Devices\\Mudra\\Calibration.txt";
        public bool FingerTipPressure;
        public bool AirMouse;
        public bool ImuQuaternion;
        public bool Gesture;

        public OnFingertipPressureEvent OnFingertipPressure;
        public OnAirMouseEvent OnAirMouse;
        public OnIMUQuaternionEvent OnImuQuaternion;
        public OnTapEvent OnTap;
        public OnDoubleTapEvent OnDoubleTap;
        public OnIndexEvent OnIndex;
        public OnThumbEvent OnThumb;
        public float MouseSpeedX =4.0f ;
        public float MouseSpeedY = 6.0f;

        Vector3 _mouseScreenCoords;
        int _screenWidth, _screenHeight;

        // Start is called before the first frame update
        void Start()
        {
            _screenWidth = Camera.main.pixelWidth;
            _screenHeight = Camera.main.pixelHeight;

            Debug.Log("TestMudraUnityPlugin started");

           try
                {
#if (UNITY_EDITOR || UNITY_STANDALONE_WIN) || NETFX_CORE || UNITY_WSA || UNITY_WSA_10_0
            Plugin.Instance.Init(CalibrationFilePath);
#elif (UNITY_ANDROID)
            Plugin.Instance.Init();
#endif
                }
            catch (System.Exception e)
                {
                    print(e.ToString());
                }
        }

        // Update is called once per frame
        void Update()
        {
            Plugin.Instance.IsGestureEnabled = Gesture;
            Plugin.Instance.IsFingerTipPressureEnabled = FingerTipPressure;
            Plugin.Instance.IsAirMouseEnabled = AirMouse;
            Plugin.Instance.IsImuQuaternionEnabled = ImuQuaternion;

            Plugin.Instance.Update();

            UpdateGestureEvent();
            UpdateFingerTipPressureEvent();
            UpdateAirMouseEvent();
            UpdateImuQuaternionEvent();
        }

        private void UpdateGestureEvent()
        {
            GestureType ? gesture = Plugin.Instance.GetLastGesture();
            if (gesture != null)
            {
                switch (gesture)
                {
                    case GestureType.DoubleTap:
                        OnDoubleTap.Invoke(true);
                        OnTap.Invoke(false);
                        OnIndex.Invoke(false);
                        OnThumb.Invoke(false);
                        break;
                    case GestureType.Tap:
                        OnDoubleTap.Invoke(false);
                        OnTap.Invoke(true);
                        OnIndex.Invoke(false);
                        OnThumb.Invoke(false);
                        break;
                    case GestureType.Index:
                        OnDoubleTap.Invoke(false);
                        OnTap.Invoke(false);
                        OnIndex.Invoke(true);
                        OnThumb.Invoke(false);
                        break;
                    case GestureType.Thumb:
                        OnDoubleTap.Invoke(false);
                        OnTap.Invoke(false);
                        OnIndex.Invoke(false);
                        OnThumb.Invoke(true);
                        break;
                }
               
            }
        }

        private void UpdateFingerTipPressureEvent()
        {
            float? fingerTipPressure = Plugin.Instance.GetLastFingerTipPressure();

            if (fingerTipPressure != null)
            {
                OnFingertipPressure.Invoke(fingerTipPressure.Value);
            }
        }
       
        private void UpdateAirMouseEvent()
        {
            float[] delta = Plugin.Instance.GetLastAirMousePositionChange();
            if (delta != null)
            {
                _mouseScreenCoords.x += delta[0] * MouseSpeedX * _screenWidth;
                _mouseScreenCoords.y -= delta[1] * MouseSpeedY * _screenHeight;
                _mouseScreenCoords.x= Mathf.Clamp(_mouseScreenCoords.x, 0.0f * _screenWidth, 1.0f * _screenWidth);
                _mouseScreenCoords.y = Mathf.Clamp(_mouseScreenCoords.y, 0.0f * _screenHeight, 1.0f * _screenHeight);
                _mouseScreenCoords.z= Camera.main.transform.position.z * -1.0f;

                OnAirMouse.Invoke(Camera.main.ScreenToWorldPoint(_mouseScreenCoords));
            }
        }
        public Vector2 getAirMouseXY()
        {
            return new Vector2(_mouseScreenCoords.x, _mouseScreenCoords.y);
        }
        private void UpdateImuQuaternionEvent()
        {
            
            Quaternion? imuQuaternion = Plugin.Instance.GetLastImuQuaternion();

            if (imuQuaternion != null)
            {
                OnImuQuaternion.Invoke(imuQuaternion.Value);               

            }
        }
        private void LateUpdate()
        {
            Plugin.Instance.ClearFrame();
        }

        private void OnApplicationQuit()
        {
            Plugin.Instance.Close();
        }
    }


    [System.Serializable]
    public class OnTapEvent : UnityEvent<bool>
    {
    }
    [System.Serializable]
    public class OnDoubleTapEvent : UnityEvent<bool>
    {
    }
    [System.Serializable]
    public class OnIndexEvent : UnityEvent<bool>
    {
    }
    [System.Serializable]
    public class OnThumbEvent : UnityEvent<bool>
    {
    }

    [System.Serializable]
    public class OnFingertipPressureEvent : UnityEvent<float>
    {
    }

    [System.Serializable]
    public class OnAirMouseEvent : UnityEvent<Vector3>
    {
    }

    [System.Serializable]
    public class OnIMUQuaternionEvent : UnityEvent<Quaternion>
    {
       // public Transform t;
    }



}


