﻿using UnityEngine;

namespace Mudra.Unity
{
    public delegate void OnLoggingMessageCallBack(string message);

    public enum GestureType
    {
        None = 0,
        Thumb = 1,
        Index = 2,
        Tap = 3,
        DoubleTap = 4
    }

    abstract public class PluginPlatform
    {
        const int NUM_OF_SNCS = 3;

        static object _lock = new object();

        static GestureType? _lastGesture = null;
        static float ? _lastFingerTipPressure;
        static float[] _lastAirMousePositionChange = null;
        static Quaternion? _lastQuaternion;

        abstract public void Init(string calibrationFile = "");
        abstract public void Update();
        abstract public void Close();

        #region OnGestureReady
        protected bool _isGestureEnabled = false;
        public bool IsGestureEnabled
        {
            get { return _isGestureEnabled; }
            set
            {
                if (_isGestureEnabled != value)
                {
                    _isGestureEnabled = value;
                    UpdateOnGestureReadyCallback();
                }
            }
        }
        abstract protected void UpdateOnGestureReadyCallback();
        #endregion

        #region OnFingerTipPressureReady
        protected bool _isFingerTipPressureEnabled = false;
        public bool IsFingerTipPressureEnabled
        {
            get { return _isFingerTipPressureEnabled; }
            set
            {
                if (_isFingerTipPressureEnabled != value)
                {
                    _isFingerTipPressureEnabled = value;
                    UpdateOnFingerTipPressureCallback();
                }
            }
        }
        abstract protected void UpdateOnFingerTipPressureCallback();
        #endregion

        #region OnAirMousePositionChanged
        protected bool _isAirMouseEnabled = false;
        public bool IsAirMouseEnabled
        {
            get { return _isAirMouseEnabled; }
            set
            {
                if (_isAirMouseEnabled != value)
                {
                    _isAirMouseEnabled = value;
                    UpdateAirMousePositionChangedCallback();
                }
            }
        }
        abstract protected void UpdateAirMousePositionChangedCallback();
        #endregion

        #region ImuQuaternion
        protected bool _isImuQuaternionEnabled = false;
        public bool IsImuQuaternionEnabled
        {
            get { return _isImuQuaternionEnabled; }
            set
            {
                if (_isImuQuaternionEnabled != value)
                {
                    _isImuQuaternionEnabled = value;
                    UpdateOnQuaternionReadyCallback();
                }
            }
        }
        abstract protected void UpdateOnQuaternionReadyCallback();
        #endregion

        abstract public bool IsConnected { get;}

        public void Clear()
        {
            lock (_lock)
            {
                _lastGesture = null;
                _lastFingerTipPressure = null;
                _lastAirMousePositionChange = null;
                _lastQuaternion = null;
            }
        }
        protected void SetLastGesture(GestureType gesture)
        {
            lock (_lock)
            {
                _lastGesture = gesture;
            }
        }

        public GestureType? GetLastGesture()
        {
            lock (_lock)
            {
                return _lastGesture;
            }
        }

        protected void SetLastFingerTipPressure(float fingerTipPressure)
        {
            lock (_lock)
            {
                _lastFingerTipPressure = fingerTipPressure;
            }
        }

        public float? GetLastFingerTipPressure()
        {
            lock (_lock)
            {
                return _lastFingerTipPressure;
            }
        }

        protected void SetLastAirMousePositionChange(float[] position)
        {
            lock (_lock)
            {
                _lastAirMousePositionChange = position;
            }
        }

        public float[] GetLastAirMousePositionChange()
        {
            lock (_lock)
            {
                return _lastAirMousePositionChange;
            }
        }

        public void SetLastQuaternion(Quaternion data)
        {
            lock (_lock)
            {
                _lastQuaternion = data;
            }
        }

        public Quaternion? GetLastImuQuaternion()
        {
            lock (_lock)
            {
                return _lastQuaternion;
            }
        }
 
    }

}
