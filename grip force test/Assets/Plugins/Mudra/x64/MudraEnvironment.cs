﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Mudra.SDK.Windows.DotNet
{
    public class MudraEnvironment
    {
        static bool _pinCallBacks = false;
        static public bool PinCallBacks
        {
            get { return _pinCallBacks; }
            set { _pinCallBacks = value; }
        }

        public const string dll_name = "MudraSDK.dll";

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void OnMudraDeviceDiscoveredCallback(string name);
        public delegate void OnMudraDeviceDiscoveredCallbackType(MudraDevice mudraDevice);
        static private OnMudraDeviceDiscoveredCallbackType m_onMudraDeviceDiscovered;

        static List<MudraDevice> _devices = new List<MudraDevice>();

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void OnLoggingMessageCallBack(string message);

        [DllImport(dll_name)]
        private static extern void EnvironmentInit();

        [DllImport(dll_name)]
        private static extern void EnvironmentScan(OnMudraDeviceDiscoveredCallback callback);

        [DllImport(dll_name)]
        private static extern void EnvironmentSetOnLoggingMessageCallback(OnLoggingMessageCallBack callback);

        [DllImport(dll_name, CharSet = CharSet.Ansi)]
        private static extern void EnvironmentConnect([MarshalAs(UnmanagedType.LPStr)] string name);

        [DllImport(dll_name, CharSet = CharSet.Ansi)]
        private static extern void EnvironmentLicense(int feature, [MarshalAs(UnmanagedType.LPStr)] string license);

        [DllImport(dll_name, CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern bool EnvironmentSetCalibration(int hand, [MarshalAs(UnmanagedType.LPStr)] string fileName);

        [DllImport(dll_name)]
        private static extern void EnvironmentSetLoggingSeverity(int severity);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void OnGestureCallbackType(int deviceId, int gesture);

        [DllImport(dll_name)]
        private static extern void EnvironmentClose();


        static MudraEnvironment()
        {
            //            LoadLib("C:\\Program Files\\Wearable Devices\\Mudra");

            EnvironmentInit();
        }

        static public void OnMudraDeviceDiscovered(string name)
        {
            var d = new MudraDevice(name);
            _devices.Add(d);
            m_onMudraDeviceDiscovered(d);
        }

        public static List<MudraDevice> Devices
        {
            get
            {
                return _devices;
            }
        }

        public static void SetLicense(Feature feature, string license)
        {
            EnvironmentLicense((int)feature, license);
        }

        public static bool SetCalibration(HandType hand, string fileName)
        {
            return EnvironmentSetCalibration((int)hand, fileName);
        }

        public static void SetLoggingSeverity(LoggingSeverity severity)
        {
            EnvironmentSetLoggingSeverity((int)severity);
        }

        static GCHandle _onLoggingMessageHandle;
        public static void SetOnLoggingMessageCallback(OnLoggingMessageCallBack callback)
        {
            if (PinCallBacks && callback != null)
            {
                _onLoggingMessageHandle = GCHandle.Alloc(callback, GCHandleType.Pinned);
            }
           
            EnvironmentSetOnLoggingMessageCallback(callback);
        }

        static GCHandle _onMudraDeviceDiscoveredHandle;
        public static void Scan(OnMudraDeviceDiscoveredCallbackType callback)
        {
            m_onMudraDeviceDiscovered = callback;
            _devices.Clear();
            EnvironmentScan(OnMudraDeviceDiscovered);
        }

        public static void Connect(MudraDevice device)
        {
            EnvironmentConnect(device.Name);
        }

        public static void Close()
        {
            EnvironmentClose();
        }



    }
}
