﻿using System;
using System.Runtime.InteropServices;

namespace Mudra.SDK.Windows.DotNet
{
    public class MudraDevice
    {
        public const string dll_name = "MudraSDK.dll";

        bool _pinCallBacks = false;
        public bool PinCallBacks
        {
            get { return _pinCallBacks; }
            set { _pinCallBacks = value; }
        }
        #region OnProportional
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void OnProportionalReadyCallBack(float proportional);

        [DllImport(dll_name, CharSet = CharSet.Ansi)]
        private static extern bool SetOnProportionalReady([MarshalAs(UnmanagedType.LPStr)] string name, OnProportionalReadyCallBack callback);

        GCHandle _onProportionalReadyHandle;
        public void SetOnProportionalReady(OnProportionalReadyCallBack callback)
        {
            if (PinCallBacks && callback != null)
            {
                _onProportionalReadyHandle = GCHandle.Alloc(callback, GCHandleType.Pinned);
            }
            SetOnProportionalReady(Name, callback);
        }
        #endregion

        #region OnGestureReady
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void OnGestureReadyCallBack(int gesture);

        [DllImport(dll_name, CharSet = CharSet.Ansi)]
        private static extern bool SetOnGestureReady([MarshalAs(UnmanagedType.LPStr)] string name, OnGestureReadyCallBack callback);

        GCHandle _onGestureReady;
        public void SetOnGestureReady(OnGestureReadyCallBack callback)
        {
            if (PinCallBacks && callback != null)
            {
                _onGestureReady = GCHandle.Alloc(callback, GCHandleType.Pinned);
            }
            SetOnGestureReady(Name, callback);
        }
        #endregion

        #region OnAirMousePositionChanged
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void OnAirMousePositionChangedCallBack(float x, float y);
        
        [DllImport(dll_name, CharSet = CharSet.Ansi)]
        private static extern bool SetOnAirMousePositionChanged([MarshalAs(UnmanagedType.LPStr)] string name, OnAirMousePositionChangedCallBack callback);

        GCHandle _onAirMousePositionChanged;
        public void SetOnAirMousePositionChanged(OnAirMousePositionChangedCallBack callback)
        {
            if (PinCallBacks && callback != null)
            {
                _onAirMousePositionChanged = GCHandle.Alloc(callback, GCHandleType.Pinned);
            }
            SetOnAirMousePositionChanged(Name, callback);
        }

        #endregion

        #region OnSncPackageReady

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void OnSncPackageReadyCallBack(int sncNum, IntPtr data);

        [DllImport(dll_name, CharSet = CharSet.Ansi)]
        private static extern bool SetOnSncPackageReady([MarshalAs(UnmanagedType.LPStr)] string name, OnSncPackageReadyCallBack callback);

        private OnSncPackageReadyCallbackType _onSncPackageReadyCallbackType;

        private void OnSncPackageReady(int sncNum, IntPtr data)
        {
            var array = GetNativeArray<float>(data, 18);

            _onSncPackageReadyCallbackType(sncNum, array);
        }

        public delegate void OnSncPackageReadyCallbackType(int sncNum, float[] data);

        public void SetOnSncPackageReady(OnSncPackageReadyCallbackType callback)
        {
            _onSncPackageReadyCallbackType = callback;
            SetOnSncPackageReady(Name, OnSncPackageReady);
        }

        #endregion

		#region ImuQuaternionPackageReady
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void OnQuaternionReadyCallBack(float x, float y, float z, float w);

        [DllImport(dll_name, CharSet = CharSet.Ansi)]
        private static extern bool SetOnImuQuaternionReady([MarshalAs(UnmanagedType.LPStr)] string name, OnQuaternionReadyCallBack callback);

        GCHandle _onQuaternionPackageReady;
        public void SetOnImuQuaternionReady(OnQuaternionReadyCallBack callback)
        {
            if (PinCallBacks && callback != null)
            {
                _onQuaternionPackageReady = GCHandle.Alloc(callback, GCHandleType.Pinned);
            }
            SetOnImuQuaternionReady(Name, callback);
        }

        #endregion


        #region SetImuNormPackageReady
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void OnImuPackageReadyCallback(IntPtr data);

		[DllImport(dll_name, CharSet = CharSet.Ansi)]
        private static extern bool SetOnImuAccNormPackageReady([MarshalAs(UnmanagedType.LPStr)] string name, OnImuPackageReadyCallback callback);
        public delegate void OnImuPackageReadyCallbackType(float[] data);

        private OnImuPackageReadyCallbackType _onImuAccNormPackageReadyCallback;
        private void OnImuNormPackageReady(IntPtr data)
        {
            var array = GetNativeArray<float>(data, 8);
            _onImuAccNormPackageReadyCallback(array);
        }

        public void SetOnImuAccNormPackageReady(OnImuPackageReadyCallbackType callback)
        {
            _onImuAccNormPackageReadyCallback = callback;
            SetOnImuAccNormPackageReady(Name, OnImuNormPackageReady);
        }
        #endregion

        #region SetHand
        [DllImport(dll_name, CharSet = CharSet.Ansi)]
        private static extern void SetHand([MarshalAs(UnmanagedType.LPStr)] string name, int hand);
        public void SetHand(HandType hand)
        {
            SetHand(Name, (int)hand);
        }
        #endregion

        #region IsConnected
        [DllImport(dll_name, CharSet = CharSet.Ansi)]
        private static extern bool IsConnected([MarshalAs(UnmanagedType.LPStr)] string name);
        public bool IsConnected()
        {
            return IsConnected(Name);
        }
        #endregion
        public string Name { get; set; }

        public MudraDevice(string name)
        {
            Name = name;
        }

        private static T[] GetNativeArray<T>(IntPtr array, int length)
        {
            T[] result = new T[length];
            int size = Marshal.SizeOf(typeof(T));

            if (IntPtr.Size == 4)
            {
                // 32-bit system
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = (T)Marshal.PtrToStructure(array, typeof(T));
                    array = new IntPtr(array.ToInt32() + size);
                }
            }
            else
            {
                // probably 64-bit system
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = (T)Marshal.PtrToStructure(array, typeof(T));
                    array = new IntPtr(array.ToInt64() + size);
                }
            }
            return result;
        }

    }
}
