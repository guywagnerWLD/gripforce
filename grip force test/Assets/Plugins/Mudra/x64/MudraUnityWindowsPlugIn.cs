﻿using Mudra.SDK.Windows.DotNet;
using UnityEngine;

namespace Mudra.Unity
{
    public class WindowsPlugin : PluginPlatform
    {
        MudraDevice _cuurentDevice;

        override public void Init(string calibrationFile)
        {
            Logger.Print("Mudra Windows Unity Plugin started");

            MudraEnvironment.PinCallBacks = true;

            MudraEnvironment.SetOnLoggingMessageCallback(OnLoggingMessage);

            MudraEnvironment.SetLoggingSeverity(LoggingSeverity.Info);

            MudraEnvironment.SetLicense(Feature.RawData, "Feature::RawData");

            if (calibrationFile != null && !MudraEnvironment.SetCalibration(HandType.Left, calibrationFile))
            {
                Logger.Print("Calibration file not found !!!!");
            }

            MudraEnvironment.Scan(OnMudraDeviceDiscovered);
        }

        override public void Close()
        {
            MudraEnvironment.Close();
        }

        void OnLoggingMessage(string msg)
        {
            Logger.Print(msg);
        }


        void OnMudraDeviceDiscovered(MudraDevice device)
        {
            _cuurentDevice = device;

            Logger.Print("OnMudraDeviceDiscovered");
            //             if (device.Name != "CA62BA4C71E3" && device.Name != "CF6C069FC6D2")
            //                 return;

            device.PinCallBacks = true;

            device.SetHand(HandType.Left);

            UpdateOnGestureReadyCallbacks(device);
            UpdateOnFingerTipPressureCallback(device);
            UpdateAirMousePositionChangedCallback(device);
            UpdateOnImuQuaternionReadyCallback(device);

            MudraEnvironment.Connect(device);
        }

        #region OnGestureReady

        void OnGestureReady(int gestureIndex)
        {
            GestureType gesture = (GestureType)gestureIndex;
            SetLastGesture(gesture);
        }

        override protected void UpdateOnGestureReadyCallback()
        {
            foreach (var d in MudraEnvironment.Devices)
            {
                UpdateOnGestureReadyCallbacks(d);
            }
        }

        private void UpdateOnGestureReadyCallbacks(MudraDevice d)
        {
            if (_isGestureEnabled)
            {
                d.SetOnGestureReady(OnGestureReady);
            }
            else
            {
                d.SetOnGestureReady(null);
            }
        }
        #endregion

        #region OnFingerTipPressureReady
        override protected void UpdateOnFingerTipPressureCallback()
        {
            foreach (var d in MudraEnvironment.Devices)
            {
                UpdateOnFingerTipPressureCallback(d);
            }
        }

        private void UpdateOnFingerTipPressureCallback(MudraDevice d)
        {
            if (_isFingerTipPressureEnabled)
            {
                d.SetOnProportionalReady(SetLastFingerTipPressure);
            }
            else
            {
                d.SetOnProportionalReady(null);
            }
        }
        #endregion

        #region OnAirMousePositionChanged
        void OnAirMousePositionChanged(float x, float y)
        {
            float[] positionChanged = new float[2] { x, y };

            SetLastAirMousePositionChange(positionChanged);
        }

        override protected void UpdateAirMousePositionChangedCallback()
        {
            foreach (var d in MudraEnvironment.Devices)
            {
                UpdateAirMousePositionChangedCallback(d);
            }
        }

        private void UpdateAirMousePositionChangedCallback(MudraDevice d)
        {
            if (_isAirMouseEnabled)
            {
                d.SetOnAirMousePositionChanged(OnAirMousePositionChanged);
            }
            else
            {
                d.SetOnAirMousePositionChanged(null);
            }
        }
        #endregion

        #region OnQuaternionReady

        void OnQuaternionReady(float x, float y, float z, float w)
        {
            Quaternion quaternion = new Quaternion( x, y, z, w );

            SetLastQuaternion(quaternion);
        }

        private void UpdateOnImuQuaternionReadyCallback(MudraDevice d)
        {
            if (_isImuQuaternionEnabled)
            {
                d.SetOnImuQuaternionReady(OnQuaternionReady);
            }
            else
            {
                d.SetOnImuQuaternionReady(null);
            }
        }
        override protected void UpdateOnQuaternionReadyCallback()
        {
            foreach (var d in MudraEnvironment.Devices)
            {
                UpdateOnImuQuaternionReadyCallback(d);
            }
        }
        #endregion


        public override void Update()
        {
        }

        public override bool IsConnected
        {
            get {
                if (_cuurentDevice == null)
                {
                    return false;
                }
                return _cuurentDevice.IsConnected();
            }
        }


    }
}


