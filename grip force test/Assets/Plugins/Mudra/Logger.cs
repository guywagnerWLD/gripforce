﻿using System.Collections.Generic;
using UnityEngine;

public class Logger : MonoBehaviour
{
    public static void Print(string msg)
    {
        lock (_backlog)
        {
            _backlog.Add(msg);
            _queued = true;
        }
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
        if (_instance == null)
        {
            _instance = new GameObject("Dispatcher").AddComponent<Logger>();
            DontDestroyOnLoad(_instance.gameObject);
        }
    }

    private void PrintMessages()
    {
        if (_queued)
        {
            lock (_backlog)
            {
                var tmp = _messages;
                _messages = _backlog;
                _backlog = tmp;
                _queued = false;
            }

            foreach (var msg in _messages)
                print(msg);

            _messages.Clear();
        }
    }

    private void Update()
    {
        PrintMessages();

    }

    private void OnApplicationQuit()
    {
        PrintMessages();
    }

    static Logger _instance;
    static volatile bool _queued = false;
    static List<string> _backlog = new List<string>(100);
    static List<string> _messages = new List<string>(100);
}