﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Mudra.Unity;

public class ConnectMudra : MonoBehaviour
{
    public string SceneToCallOnConnect;
    public GameObject LandscapImage;
    public GameObject PortraitImage;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        int swidth = Camera.main.pixelWidth;
        int sheight = Camera.main.pixelHeight;
        if (LandscapImage != null)
            LandscapImage.SetActive(swidth > sheight);
        if (PortraitImage != null)
            PortraitImage.SetActive(sheight > swidth);
        Debug.Log(Plugin.Instance.IsConnected);
        if (Plugin.Instance.IsConnected)
            SceneManager.LoadScene(SceneToCallOnConnect);
    }
}
