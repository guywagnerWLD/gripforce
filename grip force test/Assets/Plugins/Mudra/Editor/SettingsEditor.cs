﻿using UnityEditor;
using UnityEngine;
using System.IO;

namespace Mudra.Unity
{

#if (UNITY_EDITOR && UNITY_ANDROID)
    [CustomEditor(typeof(Mudra.Unity.AndroidSettings))]
#elif (UNITY_EDITOR && UNITY_STANDALONE_WIN)
    [CustomEditor(typeof(WindowsSettings))]
#endif
    public class SettingsEditor : Editor
    {
        GameObject _mudraManagerGameObject;
        MonoBehaviour _platformSettings;

#if (UNITY_EDITOR && UNITY_ANDROID)
        [SerializeField]
        string URL = "https://jitpack.io/w/user";
#endif
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

#if (UNITY_EDITOR && UNITY_ANDROID)
            Mudra.Unity.AndroidSettings myScript = (Mudra.Unity.AndroidSettings)target;
            if (GUILayout.Button("Set Token"))
            {
                FixGradleTemplate(myScript.authToken);
            }

            if (GUILayout.Button("get Token"))
            {
                Application.OpenURL(URL);
            }
#elif (UNITY_EDITOR && UNITY_STANDALONE_WIN)
            WindowsSettings myScript = (WindowsSettings)target;
#endif
        }

        [MenuItem("GameObject/Mudra Manager", false, 0)]
        private static void AddMudra()
        {
            GameObject MudraGameObject = new GameObject();
            MudraGameObject.name = "Mudra Manager";

            MudraGameObject.AddComponent<AndroidSettings>();
            MudraGameObject.AddComponent<MudraManager>();
        }

        public void FixGradleTemplate(string authToken)
        {
            if (authToken != null)
            {
                var path = Application.dataPath + "/Plugins/android/";
                var gradleFile = path + "mainTemplate.gradle";
                var gradleBackupFile = path + "mainTemplate.source";

                var gradleData = File.ReadAllText(gradleBackupFile);
                gradleData = gradleData.Replace("**TOKEN**", "'" + authToken + "'");
                File.WriteAllText(gradleFile, gradleData);
            }
        }

    }

}
