﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class calibrationtitle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnEnable()
    {
        GetComponent<Text>().text = "Participant: "+ParticipantsManager.get_participant() + "  " + ParticipantsManager.getActiveHand() + " Hand " + " Gain: "+ ParticipantsManager.getCalibration();
    }
}
