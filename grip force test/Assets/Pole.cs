﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pole : MonoBehaviour
{
    // Start is called before the first frame update
    List<Disc> discs = new List<Disc>();
    public int serial;
    public float xpos;
    public Color PoleColor;
    DiscsManager disc_manager;
    void Start()
    {
        gameObject.GetComponent<Renderer>().material.color = PoleColor;
        transform.position = new Vector3(xpos, transform.position.y, transform.position.z);
        GameObject go = GameObject.Find("DiscsManager");
        disc_manager = go.GetComponent<DiscsManager>();
        Debug.Log("");
    }

    // Update is called once per frame
    void Update()
    {
    //    Debug.Log("pole:" + serial + " count:" + discs.Count);
    }
    public void Add_Disc(Disc disc) {
        discs.Add(disc);
      //  Debug.Log("pole:" + serial + " count:" + discs.Count);
    }
    public void Remove_Disc(Disc disc) {
        discs.Remove(disc);
   //     Debug.Log("pole:" + serial + " count:" + discs.Count);
    }
    public Disc get_top_disc()
    {
       
        if (disc_manager.discs_at_rest() && discs.Count>0)
            return discs[discs.Count - 1];
        else
            return null;
    }
    public void unattach()
    {
        foreach (Disc d in discs)
        {
            d.can_attach = false;
        }
    }

    public bool discs_at_rest()
    {
        return disc_manager.discs_at_rest();
            
    }
    public bool check_order_by_serial()
    {
        int test_serial = -1;

        foreach (Disc disc in discs)
        {
            if (disc.serial > test_serial)
                test_serial = disc.serial;
            else
                return false;
        }
        return true;
    }

    public bool check_order_by_weight() {
       float weight = 1.1f;

        foreach (Disc disc in discs)
        {
            if (disc.Raise <= weight)
                weight = disc.Raise;
            else
                return false;
        }
        return true;
    }
    public int disc_count()
    {
        return discs.Count;
    }
}
