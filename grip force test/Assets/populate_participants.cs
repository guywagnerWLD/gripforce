﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class populate_participants : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnEnable()
    {
        GetComponent<Dropdown>().ClearOptions();
        List<string> s = new List<string>();
        s.Add("Select:");
        GetComponent<Dropdown>().AddOptions(s);
        GetComponent<Dropdown>().AddOptions(ParticipantsManager.get_participants());
    }
}
