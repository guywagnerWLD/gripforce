﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderMng : MonoBehaviour
{
    public GameObject fill;
    public GameObject handle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void set_color(Color color)
    {
        fill.GetComponent<Image>().color = color;
        handle.GetComponent<Image>().color = color;
    }


}
