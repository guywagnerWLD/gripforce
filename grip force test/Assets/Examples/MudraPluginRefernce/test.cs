﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class test : MonoBehaviour
{
    public bool val;
    public OnIMUQuaternionEvent OnImuQuaternion;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        OnImuQuaternion.Invoke(val);


    }
    [System.Serializable]
    public class OnIMUQuaternionEvent : UnityEvent<bool>
    {
        // public Transform t;
    }
}
