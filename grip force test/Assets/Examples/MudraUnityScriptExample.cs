﻿using UnityEngine;
using Mudra.Unity;

public class MudraUnityScriptExample : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("TestMudraUnityPlugin started");

    }

    private void UpdateGesture()
    {
        GestureType? gesture = Plugin.Instance.GetLastGesture();
        if (gesture != null)
       {
           print($" *************************  Gesture {gesture.ToString()} *******************************");
        }
    }

    private void UpdateFingerTipPressure()
    {
        float? fingerTipPressure = Plugin.Instance.GetLastFingerTipPressure();

        if (fingerTipPressure != null)
        {
            print($"FingerTipPressure {fingerTipPressure}");
        }
    }

    private void UpdateAirMousePositionChange()
    {
        float[] delta = Plugin.Instance.GetLastAirMousePositionChange();
        if (delta != null)
        {
            print($"AirMouse position change {delta[0]}, {delta[1]}");
        }
    }

    private void UpdateImuQuaternion()
    {
        Quaternion? q = Plugin.Instance.GetLastImuQuaternion();
        if (q == null)
        {
            return;
        }
        string s1 = q.Value.ToString();
        print($"Imu {s1}");
    }


    // Update is called once per frame
    void Update()
    {
        UpdateGesture();
        UpdateFingerTipPressure();
        UpdateAirMousePositionChange();
        UpdateImuQuaternion();
    }

 

}
