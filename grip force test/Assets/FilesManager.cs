using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;
using Mudra.Unity;

public class FilesManager : MonoBehaviour
{
    // Start is called before the first frame update
    static Transform[] Objects_toTrack;
    public Transform[] ojects_toTrack;
    public string pc_path;
    AndroidJavaClass jc;
    static StreamWriter outStream;
    public static string path;
    static string csv_path;
    static bool game_runing = false;
    static List<Transform> transforms = new List<Transform>();
    static string status="";
    static object _lock = new object();
    void Start()
    {
       
#if (UNITY_EDITOR || UNITY_STANDALONE_WIN) || NETFX_CORE || UNITY_WSA || UNITY_WSA_10_0
        path = pc_path;

#elif (UNITY_ANDROID)
        jc = new AndroidJavaClass("android.os.Environment");
        path = jc.CallStatic<AndroidJavaObject>("getExternalStorageDirectory").Call<String>("getAbsolutePath") + "/GripForce";
#endif


       
        System.IO.Directory.CreateDirectory(path);
        Objects_toTrack = ojects_toTrack;

        ParticipantsManager.load_participants();
        DiscsManager.LoadSessions();
    }

    public static void add_transform(Transform transform) {
        transforms.Add(transform);
    }
    public static string start_saving_game(string gameName,string trial,string participantName,string hand) {

        
        csv_path ="/" + gameName + "/"+trial + "/"+ participantName+"/"+hand;
        System.IO.Directory.CreateDirectory(path + csv_path);
        csv_path = csv_path+ "/" + System.DateTime.UtcNow.ToString().Replace("/", "_").Replace(" ", "_").Replace(":", "_") + ".csv";
        
        if (!DiscsManager.testPrecievedPressure)
        {
            outStream = System.IO.File.CreateText(path + csv_path);
            String string_to_save = "Time Stamp,";
            foreach (Transform t in Objects_toTrack)
                string_to_save += t.name + "X," + t.name + "Y,";
            foreach (Transform t in transforms)
                string_to_save += t.name + "X," + t.name + "Y,";
            string_to_save += "FingetTipPressure,Event Log";
            outStream.WriteLine(string_to_save);
            outStream.Close();
            game_runing = true;
        }
       else
        {
            outStream = System.IO.File.CreateText(path + csv_path);
            String string_to_save = "disc,actual force,percived force\n";           
            outStream.WriteLine(string_to_save);
            outStream.Close();
            game_runing = true;

        }
        return (path+csv_path);
    }

    // Update is called once per frame
    void Update()
    {
        if (game_runing && !DiscsManager.testPrecievedPressure)
        {
            String string_to_save = System.DateTime.UtcNow.TimeOfDay+ ",";
            foreach (Transform t in Objects_toTrack)
                string_to_save += t.position.x.ToString() + "," + t.position.y.ToString() + ",";
            foreach (Transform t in transforms)
                string_to_save += t.position.x.ToString() + "," + t.position.y.ToString() + ",";
            if (Plugin.Instance.GetLastFingerTipPressure() != null)
                string_to_save += Plugin.Instance.GetLastFingerTipPressure().Value.ToString() + ",";
            else string_to_save += "-,";
            string_to_save += get_status();
                  
        //    Debug.Log(string_to_save);

            outStream = System.IO.File.AppendText(path + csv_path);
            outStream.WriteLine(string_to_save);
            outStream.Close();
      //      Debug.Log("2");

      //      Debug.Log("3");
        }
    }

    public static void add_percived_measuerment(int index,float actual,float measured)
    {
        String string_to_save = index.ToString() + "," + actual.ToString() + ","+measured.ToString() + "\n";
        Debug.Log("1");
        outStream = System.IO.File.AppendText(path+csv_path);
        Debug.Log("2");
        outStream.WriteLine(string_to_save);
        Debug.Log("3");
        outStream.Close();
        Debug.Log("4");
    }
     string get_status()
    {
        lock (_lock)
        {
            string st = status;
            status = "";
            return st;
        }
       
    }

    public static string get_last_file_path()
    {
        return csv_path;
    }
    public static void set_status(string st)
    {
        lock (_lock)
            status = st;
    }
    public static void stop_saving_game()
    {
        game_runing = false;
        transforms.Clear();
    }
    public void stop_saving_game_gui()
    {
        game_runing = false;
        transforms.Clear();
    }
    public static void save_participents(string json)
    {
        string ppath = path + "/participants.json";
        
        File.WriteAllText(ppath, json);

    }
    public static void save_participents_log(string participants_log, string participants_activity)
    {
        string ppath = path + "/participants.csv";

        File.WriteAllText(path + "/participants.csv", participants_log);
        File.WriteAllText(path + "/activity_log.csv", participants_activity);

    }
    public static void save_sessions(string json)
    {
        string spath = path + "/sessions.json";
        File.WriteAllText(spath, json);

    }

    public static void save_sessions_log(string session_log)
    {
        string spath = path + "/sessions.csv";
        File.WriteAllText(spath, session_log);

    }
    public static string Load_participents()
    {
        string ppath = path + "/participants.json";
        if (File.Exists(ppath))
            return (File.ReadAllText(ppath));
        else
            return null;
    }
    public static string Load_sessions()
    {
        string spath = path + "/sessions.json";
        if (File.Exists(spath))
            return (File.ReadAllText(spath));
        else
            return null;
      
    }

    public static void save_html(string Html) {

        string html =
            "<!DOCTYPE html>\r\n" +
            "<html >\r\n" +
            "<head >\r\n" +
            "<style >\r\n" +
            "table {\r\n" +
            "font - family: arial, sans - serif;\r\n" +
            "border - collapse: collapse;\r\n" +
            "width: 100 %;\n" +
            "}\r\n" +
            "td, th {\r\n" +
            "    border: 1px solid #dddddd;\r\n" +
            " text - align: left;\r\n" +
            "    padding: 8px;\r\n" +
            "}\r\n" +
            "tr: nth - child(even) {\r\n" +
            "    background - color: #dddddd;\r\n" +
            "}\r\n" +
            "</style >\r\n" +
            "</head >\r\n" +
            "<body >\r\n" +
            "<h2 > gripForce Report </ h2 >\r\n" +
            "<table >\r\n" +
            "   <tr >\r\n" +
            "       <th > Participant </ th >\r\n" +
            "       <th > date </ th >\r\n" +
            "       <th > hand </ th >\r\n" +
            "       <th > calibration </ th >\r\n" +
            "       <th > session </ th >\r\n" +
            "       <th > trial </ th >\r\n" +
            "       <th > file location </ th >\r\n" +
            "   </tr >\r\n" +
            Html+
            "</table >\r\n" +
            "</body >\r\n" +
            "</html >\r\n";
        File.WriteAllText(path + "/ActivityLog.html", html);

    }
}
