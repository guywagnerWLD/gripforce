﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timeslidercolor : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float tl;
        tl = GetComponentInParent<Slider>().value / GetComponentInParent<Slider>().maxValue ;

        if (tl > 0.5)
            GetComponent < Image > ().color = Color.green;

        else if (tl > 0.15)
            GetComponent<Image>().color = Color.yellow;
        else
            GetComponent<Image>().color = Color.red;
    }
}
