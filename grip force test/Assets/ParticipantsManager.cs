﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticipantsManager : MonoBehaviour
{
     static Participant participant = new Participant();
    static Participants participants = new Participants();
    static int current_participant;
    static Hand ActiveHand;
    // Start is called before the first frame update
    void Start()
    {


    }

    public void loadParticipants() {
        load_participants();

    }
    static public string getActiveHand()
    {
        return ActiveHand.ToString();
    }

    static public string getDominantHand()
    {
        return participant.dominant_hand.ToString();
    }

    static public void load_participants()
    {
        string json = FilesManager.Load_participents();
        if (json != null)
        {
            participants = JsonUtility.FromJson<Participants>(json);
        }
    }
    string pressure_evaluation = "Actual Grip Force,Percieved force,bar visible\n";

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setActiveHand(int hand)
    {
        ActiveHand = (Hand)(hand-1);
    }

    public void setActiveHandtoDominant()
    {
        ActiveHand = participant.dominant_hand;
    }

    public void SetName(string name)
    {   
       participant.name = name;
            }
    public void SetId(string id)
    {
        participant.id = id;
    }
    public void SetAge(string Age)
    {
        participant.Age = Age;
    }

    public void SetHand(int hand)
    {
        participant.dominant_hand = hand!=0? (Hand)(hand-1):Hand.Left;

    }

    public void SetGender(string gender)
    {
        participant.gender = gender;
    }

    public void SetGender(int gender)
    {
        if(gender==1)
            participant.gender = "Male";
        else if (gender == 2)
            participant.gender = "Female";
    }
    public void select(int n)
    {
        current_participant = n-1;
    }
    public void setCalibration(float cal)
    {
        if (ActiveHand==Hand.Left)
          participants.ParticipantList[current_participant].Left_calibration = cal;  
        else if (ActiveHand == Hand.Right)
            participants.ParticipantList[current_participant].Right_calibration = cal;

    }

    public static float getCalibration()
    {
        if (ActiveHand == Hand.Left)
            return participants.ParticipantList[current_participant].Left_calibration;
        else if (ActiveHand == Hand.Right)
            return participants.ParticipantList[current_participant].Right_calibration;
        else return 1;
    }

    public void AddParticipant()
    {
        if (participant.name == "")
        {
            LogManager.message("please name the participant");
            return;
        }
        foreach (Participant pp in participants.ParticipantList)
            if (pp.name == participant.name)
            {
                LogManager.message("Participant " + pp.name +" Allready exists");
                return;
            }
        Participant p = new Participant();
        p.name = participant.name;
        p.id = participant.id;
        p.Age = participant.Age;
        p.dominant_hand = participant.dominant_hand;
        p.gender = participant.gender;
        participants.ParticipantList.Add(p);
        save_participants_json();
        current_participant = participants.ParticipantList.Count - 1;
    }

    
    public static List<string> get_participants() {
        List<string> s = new List<string>();
        foreach (Participant p in participants.ParticipantList)
            s.Add(p.name + "_" + p.created);
        return (s);
    }

    public static String get_participants(int n)
    {
        return (participants.ParticipantList[n - 1].name + "_" + participants.ParticipantList[n - 1].created); ;
    }

    public static String get_participant()
    {
        return (participants.ParticipantList[current_participant].name + "_" + participants.ParticipantList[current_participant].created); ;
    }

    [Serializable]
    public class Participant
    {        
        public string created;
        public string name;
        public string id;
        public string Age;
        public Hand dominant_hand;
        public float Left_calibration = 1;
        public float Right_calibration = 1;
        public string gender;
        public List<SavedGamesParams> SavedGames = new List<SavedGamesParams>();
        public Participant()
        {
            created=DateTime.Now.ToString("MM_dd");
        }
    }

    public  void Save_Game()
    {
        SavedGamesParams sg = new SavedGamesParams();
        sg.ActiveHand = ActiveHand;
        sg.participantName = participants.ParticipantList[current_participant].name ;
        sg.calibration=getCalibration();
        sg.SaveGame();
        participants.ParticipantList[current_participant].SavedGames.Add(sg);
        save_participants_json();
    }

    public  void save_participants_json()
    {
        string json = JsonUtility.ToJson(participants);
        FilesManager.save_participents(json);
        save_participants_log();
    }
    void save_participants_log()
    {
        string participants_log = "name,created,id,gender,dominant hand,Left Calibration,Right Calibration,age\n";
        string html = "";
           string activity_log="Participant,date,hand,calibration,session,trial,   \n";
        foreach (Participant p in participants.ParticipantList)
        {
            participants_log += p.name + "," + p.created + "," + p.id + "," +
                p.gender + "," + p.dominant_hand + "," +p.Left_calibration+","+p.Right_calibration+"," + p.Age + "\n";
            foreach (SavedGamesParams sgp in p.SavedGames)
            {
                activity_log += p.name + "," + sgp.dateTime + "," +
                     sgp.ActiveHand + ","+sgp.calibration+"," + sgp.sesion_name + "," + sgp.trial_id + "," + sgp.path + "\n";
                html +=
                   "<tr>\n"+
                   " <td>" + p.name + "</td>\r\n" +
                   " <td>" + sgp.dateTime + "</td>\r\n" +
                   " <td>" + sgp.ActiveHand + "</td>\r\n" +
                   " <td>" + sgp.calibration + "</td>\r\n" +
                   " <td>" + sgp.sesion_name + "</td>\r\n" +
                   " <td>" + sgp.trial_id + "</td>\r\n" +
                   " <td><a href = \"file://" + sgp.path + "\" >" + sgp.path + " </ a ></ td >\r\n" +
                   "</tr>\r\n";
            }
        }


        FilesManager.save_participents_log(participants_log, activity_log);
        FilesManager.save_html(html);



    }
    [Serializable]
    public class Participants
    {
        public List<Participant> ParticipantList = new List<Participant>();
    }

    [Serializable]
    public enum Hand {
        Left,
        Right
    }
    [Serializable]
    public class SavedGamesParams
    {
        public int sesion_id;
        public string sesion_name;
        public int trial_id;
        public DateTime dateTime;
        public Hand ActiveHand;
        public float calibration;
        public string path;
        public string participantName;
        public void  SaveGame()
        {
            sesion_id = DiscsManager.get_current_game_index();
            sesion_name = DiscsManager.get_current_game_name();
            trial_id = DiscsManager.get_current_trial();
            dateTime = DateTime.Now;

            path= FilesManager.start_saving_game(sesion_name, trial_id.ToString(), participantName, ActiveHand.ToString());

        }
    }
}
