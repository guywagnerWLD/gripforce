using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mudra.Unity;

public class Disc : MonoBehaviour
{
    enum State  { rest,raising,raised,droped,kiled} ;
    
    State DiscState;
    State NextState;
    public int serial;
    public Color DiscColor;
    public float Raise;
    public float release;
    public float BreakPoint;    
    public float greep_force;
    public float DiscSize;
    public bool relect_raise;
    float floaing_zone=3.0f;
    float droped_zone= 1.0f;
    bool pole_colision = false;
    Pole pole;   
    public bool touched_ground = false;
    public bool can_attach = false;
    float xpos;
  //  cursor mycursor;
    


    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponentInChildren<ParticleSystem>().Stop();
        gameObject.GetComponent<Renderer>().material.color = DiscColor;
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        gameObject.GetComponent<Rigidbody>().useGravity = true;
      //  if (cursor!= null)
        cursor.remove_disc(this);
        transform.position = new Vector3(-7, 3.5f, 33);
        if (relect_raise) 
                 transform.localScale = new Vector3(DiscSize + 2* BreakPoint, transform.localScale.y,DiscSize + 2 * BreakPoint);
        else
            transform.localScale = new Vector3(DiscSize , transform.localScale.y, DiscSize);

        DiscState = State.droped;
        greep_force = 0;
        gameObject.GetComponentInChildren<ParticleSystem>().Stop();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        var currentVelocity = gameObject.GetComponent<Rigidbody>().velocity;
        if (currentVelocity.y <= 0f)
            return;
        currentVelocity.y=0;
        gameObject.GetComponent<Rigidbody>().velocity = currentVelocity;
    }

    public bool discOk()
    {
        return DiscState != State.kiled;
    }
    void Update()
    {

        int cursurs_disc =  cursor.get_disc() == null ? -1 : cursor.get_disc().serial;
        int pole_n = pole == null ? -1 : pole.serial;

        Debug.Log("Discs line 67 : disc: " + this.serial.ToString() + " state: " + DiscState + " pole: " + pole_n.ToString() + " colidided with pole? " + pole_colision.ToString() + " cursers holds a disc " + cursurs_disc.ToString() +" v =" + gameObject.GetComponent<Rigidbody>().velocity);

        if (pole_colision && transform.position.y < droped_zone && gameObject.GetComponent<Rigidbody>().IsSleeping()) ;// (gameObject.GetComponent<Rigidbody>().velocity.y <= 0.3f) && (gameObject.GetComponent<Rigidbody>().velocity.y >= -0.3f))
            touched_ground = true;
       // Debug.Log("touch ground= " + touched_ground);
        float? fingerTipPressure = Plugin.Instance.GetLastFingerTipPressure();
        if (fingerTipPressure != null)
            greep_force = Mathf.Min(fingerTipPressure.Value*ParticipantsManager.getCalibration(),1.0f);
     //   Debug.Log("greep_force: " + greep_force.ToString());
            //  Debug.Log(NextState);
            switch (DiscState)
        {
            case State.droped:
              
                    cursor.hold_cursor((float)transform.position.x);

                if (pole_colision)
                {

                    transform.localPosition = new Vector3(pole.transform.position.x,
                                                    transform.localPosition.y,
                                                    transform.localPosition.z);

                    if (touched_ground)
                    {

                        cursor.remove_disc(this);
                        pole.Add_Disc(this);
                        FilesManager.set_status("disc " + serial.ToString() + "rests on pole " + pole.serial.ToString());
                        NextState = State.rest;
                    }
                    else
                    {                      
                        NextState = State.droped;
                    }
                }
                else
                {
                    FilesManager.set_status("disc " + serial.ToString() + "droped on the ground");
                    NextState = State.droped;
                }
                break;
            case State.rest:
                if ( can_attach && cursor.get_disc() == null && greep_force >= Raise)
                {
                    FilesManager.set_status("disc " + serial.ToString() + "is being raised  from pole " + pole.serial.ToString());
                    touched_ground = false;
                    NextState = State.raising;
                    bool success = false;
                        success = cursor.set_disc(this);
                }
                else
                {
                  
                    NextState = State.rest;
                }
                break;
            case State.raising:
                   raise_disk();
                break;
            case State.raised:                
                if (greep_force <= release)
                {                   
                    gameObject.GetComponent<Rigidbody>().useGravity = true;                    
                    NextState = State.droped;
                    FilesManager.set_status("disc " + serial.ToString() + "droped on pole " + pole.serial.ToString());

                }
                else if (greep_force >= BreakPoint)
                {
                    FilesManager.set_status("disc " + serial.ToString() + "reached braking point");
                    destroy_disc();
                    NextState = State.kiled;
                    //     Debug.Log("blow");
                }
                else
                {
                 //   if (cursor.Instance != null)
                 //   {
                        var pos = cursor.airmouse();
                        pos.y = floaing_zone - (Raise - greep_force);
                        transform.position = pos;
                //    }

                    NextState = State.raised;
                }
                break;
            case State.kiled:
                FilesManager.set_status("disc " + serial.ToString() + "destroyed");
                greep_force = 0;
             //   NextState = State.rest;

                break;
        }
       
        //Debug.Log(DiscState+" " + NextState);
        DiscState = NextState;

        
    }

    void raise_disk()
    {
        if (greep_force >= Raise)
        {
            if (transform.position.y < floaing_zone)
            {
            
                cursor.hold_cursor((float)transform.position.x);
                gameObject.GetComponent<Rigidbody>().useGravity = false;
                transform.Translate(Vector3.up * 0.1f);
                NextState = State.raising;
                //    Debug.Log(transform.position.y);
            }
            else
            {

  //              if (pole != null)
  //              {
                    NextState = State.raised;
                    pole.Remove_Disc(this);
 //               } 
              /*  if (pole_colision == false)                {
                  
                    pole = null;

                }*/
                   

            }
        }
        else
        {
          //  mycursor.remove_disc(this);
            gameObject.GetComponent<Rigidbody>().useGravity = true;
            NextState = State.rest;
        }

    }
    
    void destroy_disc()
    {        
        var exp = gameObject.GetComponentInChildren<ParticleSystem>();
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<Rigidbody>().useGravity=false;
        exp.Play();
       
        cursor.remove_disc(this);
        //transform.position = new Vector3(-7, 3.5f, 33);
        Invoke("Start", exp.duration + 5.0f);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Pole>()&& !pole_colision)
        {           
            pole_colision = true;
            pole = other.GetComponent<Pole>();
          //  Debug.Log("entered pole" + pole.serial);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (pole != null && other.GetComponent<Pole>())
            if (other.GetComponent<Pole>().serial == pole.serial)
            {
           //     Debug.Log("exit pole"+pole.serial);
                pole_colision = false;
            //    pole = null;
            }
    }
    private void OnCollisionEnter(Collision collision)
    {
        collision.impulse.Set(0, 0, 0);
        if (transform.position.y < droped_zone && !pole_colision) { 
            destroy_disc();
            DiscState = State.kiled;
            NextState = State.kiled;
        }
        else
            if (transform.position.y < droped_zone && pole_colision)
        {
            touched_ground = true;
            cursor.remove_disc(this);

        }


        }

    public bool is_at_rest()
    {
        
        return touched_ground&& (DiscState == State.rest);
    }

  //  public void set_cursor(cursor acursor)
 //   {
  //      cursor.Instance = acursor;

  //  }

}
