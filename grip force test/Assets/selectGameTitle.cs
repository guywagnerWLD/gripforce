﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class selectGameTitle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = "Participant: "+ParticipantsManager.get_participant() + "  " + ParticipantsManager.getActiveHand() + 
            " Hand " + "  Gain: " + ParticipantsManager.getCalibration()+"  Session: " + DiscsManager.get_current_game_name() + "   Trial" + DiscsManager.get_current_trial();
    }

}

